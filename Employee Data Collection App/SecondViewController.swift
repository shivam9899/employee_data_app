//
//  SecondViewController.swift
//  Employee Data Collection App
//
//  Created by daffolap on 24/08/20.
//  Copyright © 2020 daffolap. All rights reserved.
//

import UIKit

protocol AgeDelegate {
    func showAge(age: String)
}
class SecondViewController: UIViewController {
    var delegate: AgeDelegate?
    var displayNameText: String! = ""
    var displayNumberText: String! = ""
    func createAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var displayContent: UILabel!
    
    @IBOutlet weak var displayNumContent: UILabel!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Second Screen"
        displayContent.text = displayNameText
        displayNumContent.text = displayNumberText

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backToHomeScreen(_ sender: UIButton) {
        guard ageTextField.text?.count != 0  else {
            createAlert(title: "Warning!", message: "PLease enter your age before going back to home screen")
            return }
        delegate?.showAge(age: ageTextField.text!)
        dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
