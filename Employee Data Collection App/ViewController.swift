//
//  ViewController.swift
//  Employee Data Collection App
//
//  Created by daffolap on 24/08/20.
//  Copyright © 2020 daffolap. All rights reserved.
//

import UIKit

class ViewController: UIViewController,AgeDelegate {
    func showAge(age: String) {
        ageLabel.text = "Age: \(age)"
    }
    

    @IBOutlet weak var validationMessage: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var numberField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home Screen"
        validationMessage.isHidden = true
        // Do any additional setup after loading the view.
    }
    func createAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func displayButtonPressed(_ sender: UIButton) {
        let (name,number) = validtion()
        if name != nil && number != nil{
            pushViewController(nameText: name!, numberText: number!)
        }
        
    }
    
    func validtion() -> (String?,String?){
        validationMessage.isHidden = true
        guard let nameText = nameField.text, nameField.text?.count ?? 0 >= 8  else {
            validationMessage.isHidden = false
                createAlert(title: "Warning!", message: "Please Enter your name (8-30 characters)")
            validationMessage.text = "Please Enter your name (8-30 characters)"
            return (nil,nil)
        }
        
        guard let numberText = numberField.text, numberField.text?.count == 10 else {
            validationMessage.isHidden = false
            createAlert(title: "Warning!", message: "Please Enter your number (10 digit number)")
            validationMessage.text = "Please Enter your number (10 digit number)"
            return (nil,nil)
        }
        
        return (nameText,numberText)
    }
    
    func pushViewController(nameText: String, numberText: String) {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        secondVC.displayNameText = nameText
        secondVC.displayNumberText = numberText
        secondVC.delegate = self
        
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
    
}

